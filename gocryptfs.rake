=begin

Set of rake tasks for mounting/unmounting and querying status of gocryptfs
(https://nuetzlich.net/gocryptfs) filesystems (specified by FILESYSTEMS hash).

Relies on presence of gocryptfs(1) and fusermount(1) command-line tools.

=end

require 'json'
require_relative './lib/gocryptfs/fs'

#
# Adding a new password to file:
#
# echo -n secret \
# | gpg --encrypt --recipient vderyagin@gmail.com --output - \
# | base64 --wrap=0
#

FS_LIST_FILE = File.expand_path('gocryptfs.json', ENV['XDG_CONFIG_HOME'])

def get_filesystems
  GocryptFS::FS.from_config(FS_LIST_FILE)
rescue
  warn 'gocryptfs fs list file not found'
  return []
end

FILESYSTEMS = get_filesystems().freeze()

namespace :gocryptfs do
  FILESYSTEMS.each do |fs|
    namespace fs.name do
      if File.exist?(fs.store)
        desc "Mount gocryptfs filesystem \"#{fs.name}\""
        task :mount do
          fs.mount
          Rake::Task["gocryptfs:#{fs.name}:status"].invoke
        end

        desc "Unmount gocryptfs filesystem \"#{fs.name}\""
        task :umount do
          fs.umount
          Rake::Task["gocryptfs:#{fs.name}:status"].invoke
        end

        desc "Tell whether gocryptfs filesystem \"#{fs.name}\" is mounted"
        task :status do
          puts fs
        end
      else
        desc "Create \"#{fs.name}\" gocryptfs filesystem unless already exists"
        task :new do
          fs.create or warn "failed to create filesystem \"#{fs.name}\""
        end
      end
    end
  end

  if FILESYSTEMS.map(&:store).any?(&File.method(:exists?))
    desc 'Report status of each gocryptfs filesystem'
    task :status do
      FILESYSTEMS
        .select { |fs| File.directory?(fs.store) }
        .each do |fs|
          Rake::Task["gocryptfs:#{fs.name}:status"].invoke
        end
    end

    desc 'Unmount all mounted gocryptfs filesystem'
    task :umount do
      FILESYSTEMS
        .select { |fs| File.directory?(fs.store) }
        .each do |fs|
          Rake::Task["gocryptfs:#{fs.name}:umount"].invoke
        end
    end
  end
end
