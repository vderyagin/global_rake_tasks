# frozen_string_literal: true

require 'English'
require 'base64'
require 'fileutils'
require 'json'
require 'mkmf'

module MakeMakefile::Logging
  @logfile = File::NULL         # don't write mkmf.log
  @quiet = true                 # don't write to stdout either
end

module GocryptFS
  ASKPASS = File.expand_path('gocryptfs-askpass', __dir__)

  class FS
    attr_reader :name, :mount_dir, :store

    def self.from_config(config_file_path)
      read_config(config_file_path)
        .map { |name, settings|
          new(
            name,
            settings['store'],
            settings['mount_dir'],
            encrypted_password: settings['password']
          )
        }
    end

    def self.read_config(config_file_path)
      JSON.parse(IO.read(config_file_path))
    end

    def initialize(name, store, mount_dir,
                   encrypted_password: nil,
                   plain_text_password: nil)
      @name = name
      @store = File.expand_path(store)
      @mount_dir = File.expand_path(mount_dir)
      @encrypted_password = encrypted_password
      @plain_text_password = plain_text_password

      unless (!!@encrypted_password) ^ (!!@plain_text_password)
        fail 'only one password can be specified (encrypted or plain text)'
      end
    end

    def mounted?
      `mount`.include?(@mount_dir)
    end

    def password
      return @plain_text_password if @plain_text_password

      IO.popen(%W(gpg --decrypt --quiet), 'r+', err: '/dev/null') do |pipe|
        pipe.write Base64.decode64(@encrypted_password)
        pipe.close_write
        pipe.read
      end
    end

    def mount
      return true if mounted?

      Dir.mkdir @mount_dir unless File.exist?(@mount_dir)
      cmd = ['gocryptfs', store, mount_dir]
      cmd << '-extpass' << ASKPASS << '-extpass' << name if name

      IO.popen(cmd, 'r+') do |pipe|
        pipe.puts @plain_text_password if @plain_text_password

        pipe.close_write
        pipe.read
      end

      unless $CHILD_STATUS.success?
        Dir.rmdir @mount_dir
        fail "Failed to mount #{@mount_dir}"
      end
    end

    def umount
      unless mounted?
        cleanup
        return true
      end

      command =
        find_executable('fusermount') ? %w[fusermount -uz] : %w[umount -f]

      system(*command, @mount_dir).tap { |ok| cleanup if ok }
    end

    def cleanup
      Dir.rmdir @mount_dir if File.exist?(@mount_dir)
    end

    def inside(&block)
      create unless exist?
      mount unless mounted?
      Dir.chdir mount_dir, &block
    ensure
      umount
    end

    def to_s
      @mount_dir + ' is ' +
        if mounted?
          "\e[1m\e[32mMOUNTED\e[0m"
        else
          "\e[1m\e[31mNOT MOUNTED\e[0m"
        end
    end

    def exist?
      File.exist?(@store) && Dir.entries(@store).length > 2
    end

    def create
      return true if mounted?

      if exist?
        warn "#{@store} exists and is not empty"
        return
      end

      FileUtils.mkdir_p [@store, @mount_dir]

      pw = password()

      IO.popen(%W(gocryptfs -init #{@store}), 'r+') do |pipe|
        2.times do
          pipe.puts pw
        end

        pipe.close_write
        pipe.read
      end
    end
  end
end
