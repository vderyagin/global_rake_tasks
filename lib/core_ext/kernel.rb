require 'tmpdir'

module Kernel
  private

  def in_gitlab_repo(repo, &block)
    in_git_repo "git@gitlab.com:#{repo}.git", &block
  end

  def in_github_repo(repo, &block)
    in_git_repo "https://github.com/#{repo}.git", &block
  end

  def in_git_repo(uri, &block)
    repo_dir = uri[%r{(?<=/)[^/]+\z}].chomp('.git') || 'git_repo'

    in_temporary_directory do
      system 'git', 'clone', uri, repo_dir
      Dir.chdir repo_dir, &block
    end
  end

  def in_temporary_directory(&block)
    Dir.mktmpdir do |tmpdir|
      Dir.chdir tmpdir, &block
    end
  end

  def in_encrypted_directory(&block)
    require_relative '../gocryptfs/fs'
    require 'securerandom'

    Dir.mktmpdir do |store|
      GocryptFS::FS.new(
        nil,
        store,
        Dir.mktmpdir,
        plain_text_password: SecureRandom.base64(40)
      ).inside(&block)
    end
  end
end
