require_relative './lib/core_ext/kernel'

def flush_caches
  ENV['SUDO_ASKPASS'] = '/usr/bin/ssh-askpass-fullscreen'
  sh 'sync && echo 3 | sudo --askpass tee /proc/sys/vm/drop_caches'
end

def ensure_config
  return if File.exist?(File.expand_path('~/.emacs.d/init.el'))

  compile_config
end

def compile_config(upgrade_packages = nil)
  command = %w(./compile-configuration).tap do |cmd|
    cmd << '--upgrade-packages' if upgrade_packages
  end

  in_gitlab_repo 'vderyagin/dotemacs' do
    sh(*command)
  end
end

def profile_init
  in_github_repo 'juergenhoetzel/profile-dotemacs' do
    sh 'emacs', '--no-init-file',
       '--eval', '(setq profile-dotemacs-low-percentage 1 gc-cons-threshold 80000000)',
       '--load', 'profile-dotemacs.el',
       '--load', 'subr-x',
       '--funcall', 'profile-dotemacs'
  end
end

# Return list of full paths of content of dir (not recursive, includes all
# files & directories, excludes "." & "..").
def entries_in(dir)
  Dir
    .foreach(dir)
    .grep_v(/\A\.\.?\z/)
    .map { |file| File.expand_path(file, dir) }
end

namespace :emacs do
  desc 'Compile Emacs configuration'
  task :init_file do
    compile_config
  end

  desc 'Profile Emacs startup'
  task :profile do
    ensure_config
    profile_init
  end

  desc 'Profile Emacs startup (cold)'
  task :profile_cold do
    ensure_config
    flush_caches
    profile_init
  end

  desc 'Update emacs config, theme and packages'
  task :update do
    compile_config :upgrade_packages
  end

  desc 'Clean up org files'
  task :org_clean do
    ORG_BACKUP_DIR = File.expand_path('org.bak', '~')

    if File.exist?(ORG_BACKUP_DIR)
      abort "#{ORG_BACKUP_DIR} location is taken (needed for backup)"
    end

    org_repo = 'gcrypt::git@gitlab.com:vderyagin/org.git'

    Rake::Task['gocryptfs:org:mount'].invoke

    Dir.chdir File.expand_path('~/org') do
      sh 'git', 'sync'
    end

    in_encrypted_directory do
      sh 'git', 'clone', '--mirror', org_repo

      Dir.chdir 'org.git' do
        sh 'bfg', '--delete-files', 'inbox.org'
        sh 'bfg', '--delete-files', '*.gpg'

        sh 'git', 'reflog', 'expire', '--expire=now', '--all'
        sh 'git', 'gc', '--prune=now', '--aggressive'

        ENV['GCRYPT_FULL_REPACK'] = 'true'
        sh 'git', 'push', '--force'
      end
    end

    Dir.chdir File.expand_path('~') do
      sh 'git', 'clone', org_repo, 'org.new'
      cp_r 'org', ORG_BACKUP_DIR, verbose: true
      rm_rf entries_in('org'), verbose: true
      mv entries_in('org.new'), 'org', verbose: true
      rmdir 'org.new', verbose: true
    end

    Dir.chdir File.expand_path('~/org') do
      sh 'git', 'config', '--bool', 'branch.master.sync', 'true'
      sh 'git', 'gc', '--prune=now', '--aggressive'
    end
  end
end

desc 'Update emacs config'
task :emacs do
  compile_config
end
